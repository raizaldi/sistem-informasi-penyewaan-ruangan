﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;


namespace penyewaan_ruangan.Admin
{
    public partial class Jadwal : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksi"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["username"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            tampilruang();
        }

        public void tampilruang()
        {
            string sql = @"SELECT a.no_penyewaan as [no]
                          ,a.no_ruangan as [ruang]
                          ,a.tgl_pemesanan as [tanggal]
                          ,a.id_customer as [id]
                          ,a.total_bayar as [jumlah]
                          ,b.nama_customer as [nama_customer]
                      FROM [penyewaan] a inner join [customer] b on a.id_customer=b.id_customer";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            ListJadwal.DataSource = dt;
            ListJadwal.DataBind();
            cmd.Dispose();
            conn.Close();
        }
    }
}