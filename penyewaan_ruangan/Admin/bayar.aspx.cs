﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace penyewaan_ruangan.Admin
{
    public partial class bayar : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksi"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["username"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            nilaiTertinggi();
            if (!IsPostBack) {
                noruangan.Visible = false;
                info.Visible = false;
            }
        }

        public void nilaiTertinggi()
        {
            try
            {
                int hitung;
                string urut;
                conn.Open();
                string sql = @"SELECT count(no_bayar) as no FROM [bayar]";
                cmd = new SqlCommand(sql, conn);
                dr = cmd.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    urut = dr["no"].ToString();
                    hitung = int.Parse(urut);
                    hitung = hitung + 1;
                    no.Text = hitung.ToString();

                }
                else
                {
                    no.Text = "1";
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                no.Text = ex.Message;
            }
        }

        public void simpanjadwal() {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            noruangan.Visible = true;
            cmd.CommandText = @"INSERT INTO [jadwal]([no_ruangan],[no_bayar],[keterangan])VALUES('"+noruangan.Text+"','"+no.Text+"','Sudah Lunas Membayar')";
            cmd.ExecuteNonQuery();
           
        }

        public void simpanbayar() {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"INSERT INTO [bayar]
           ([no_bayar]
           ,[no_penyewaan]
           ,[tgl_bayar]
           ,[jml_bayar]) VALUES ('" + no.Text + "','" + nosewa.Text + "','" + tanggal.Text + "','" + jumlah.Text + "')";
            cmd.ExecuteNonQuery();
           
        }




        protected void btnsimpan_Click(object sender, EventArgs e)
        {
            conn.Open();
            simpanbayar();
            simpanjadwal();
            nosewa.Text = "";
            tanggal.Text = "";
            jumlah.Text = "";
            noruangan.Text = "";
            buktibayaran();
            noruangan.Visible = false;
            conn.Close();
           
            info.Visible = true;
            info.Text = "Pembayaran berhasil";

        }

        protected void cari_Click(object sender, EventArgs e)
        {
            conn.Open();
            string sql = @"SELECT [no_ruangan],[total_bayar] FROM [penyewaan] where [no_penyewaan]='" + nosewa.Text + "'";
            cmd = new SqlCommand(sql, conn);
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    noruangan.Text =  dr["no_ruangan"].ToString();
                    jumlah.Text = dr["total_bayar"].ToString();
                }
            }
            conn.Close();
        }

        public void buktibayaran()
        {
            string sql = @"select bayar.no_bayar as no_bayar,penyewaan.no_penyewaan as no_sewa,customer.nama_customer,bayar.tgl_bayar ,bayar.jml_bayar
from bayar,penyewaan,customer where bayar.no_penyewaan=penyewaan.no_penyewaan
and penyewaan.id_customer=customer.id_customer and bayar.no_bayar='" + no.Text + "'";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            buktipembayaran.DataSource = dt;
            buktipembayaran.DataBind();
           
        }

    }
}