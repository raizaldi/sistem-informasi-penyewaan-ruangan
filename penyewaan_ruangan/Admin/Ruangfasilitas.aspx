﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Ruangfasilitas.aspx.cs" Inherits="penyewaan_ruangan.Admin.Ruangfasilitas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		
			<div class="col-lg-12">
				<h2 class="page-header"><center>Halaman Ruang dan Fasilitas</center></h2>
                 <marquee>Sistem Informasi Pengelolaan penyewaan ruangan</marquee>
			</div>
            <div class="col-lg-4">
                <div class="form-group">
                    <asp:TextBox ID="no" runat="server" class="form-control" placeholder="No Ruang" Width="300px" autofocus></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="nama"  runat="server" class="form-control" placeholder="Nama Ruang" Width="300px" ></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="harga" runat="server" class="form-control" placeholder="Harga" Width="300px" ></asp:TextBox>
                </div>
                <div class="form-group">
                     <asp:FileUpload class="form-control" ID="uploadgambar" runat="server"/>
                </div>
                
                <asp:Button ID="btnsimpan" runat="server" Text="Simpan" class="btn btn-success" 
                    onclick="btnsimpan_Click" />
                &nbsp;
               <asp:Button ID="btnubah" runat="server" Text="Ubah" class="btn btn-warning" 
                    Width="84px" onclick="btnubah_Click" />
                 &nbsp;
               <asp:Button ID="btnhapus" runat="server" Text="Hapus" class="btn btn-danger" 
                    onclick="btnhapus_Click" />
            </div>
            <div class="col-lg-8">
            <asp:ListView ID="ListFasilitas" runat="server">
            <LayoutTemplate>
                <table class="table table-bordered">
                    <thead>
                    <th class="danger"><center>No Ruang</center></th>
                    <th class="danger"><center>Nama Ruang</center></th>
                    <th class="danger"><center>Harga</center></th>
                    <th class="danger"><center>Foto</center></th>
                    <th class="danger">Aksi</th>
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td align="center"><%# Eval("no_ruangan")%></td>
                    <td align="center"><%# Eval("nama_ruangan")%></td>
                    <td align="center"><%# Eval("harga_ruangan")%></td>
                    <td align="center"><img src="../images/<%# Eval("foto")%>" width="50px" height="30px" class="img-rounded"></td>
                  
                    <td>
                        <asp:LinkButton class="btn btn-info" ID="Inkselect" runat="server" CommandArgument='<%# Eval("no_ruangan")%>' OnClick="Inkpilih_Click" >Pilih</asp:LinkButton>
                    </td>
                </tr>
               
            </ItemTemplate>
        </asp:ListView>

            </div>
		</div><!--/.row-->
</div>
</asp:Content>
