﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;


namespace penyewaan_ruangan.Admin
{
    public partial class Laporan : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksi"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["username"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            tampilruang();
            tampiltotal();
        }

        public void tampilruang()
        {
            string sql = @"SELECT no_bayar
                          ,no_penyewaan
                          ,tgl_bayar
                          ,jml_bayar
                          ,(select SUM(jml_bayar) from bayar) as total 
                      FROM bayar ";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            ListJadwal.DataSource = dt;
            ListJadwal.DataBind();
            cmd.Dispose();
            conn.Close();
        }

        public void tampiltotal()
        {
            string sql = @"select SUM(jml_bayar) as total FROM bayar ";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            listTotal.DataSource = dt;
            listTotal.DataBind();
            cmd.Dispose();
            conn.Close();
        }
    }
}