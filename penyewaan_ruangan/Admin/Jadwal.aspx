﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Jadwal.aspx.cs" Inherits="penyewaan_ruangan.Admin.Jadwal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="row">
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
<div id="print-area">		
			<div class="col-lg-12">
				<h2 class="page-header"><center>Halaman Jadwal Sewa</center></h2>
                 <marquee>Sistem Informasi Pengelolaan penyewaan ruangan</marquee>
			</div>

              <div class="col-lg-12">
            <asp:ListView ID="ListJadwal" runat="server">
            <LayoutTemplate>
                <table class="table table-bordered">
                    <thead>
                    <th class="info"><center>No Jadwal</center></th>
                    <th class="info"><center>No Ruang</center></th>
                    <th class="info"><center>Tanggal Sewa</center></th>
                    <th class="info"><center>Id Customer</center></th>
                    <th class="info"><center>Nama Customer</center></th>
                   
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td align="center"><%# Eval("no")%></td>
                    <td align="center"><%# Eval("ruang")%></td>
                    <td align="center"><%# Eval("tanggal")%></td>
                    <td align="center"><%# Eval("id")%></td>
                    <td align="center"><%# Eval("nama_customer")%></td>
                </tr>
               
            </ItemTemplate>
        </asp:ListView>

            </div>
       
</div>
<div class="col-lg-12">
 <button type="button" onclick="printDiv('print-area')" class="btn btn-success pull-right"><i class="fa fa-print"></i> Print atau cetak pdf
          </button>
</div>
</div><!--/.row-->

</div>


</asp:Content>
