﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="bayar.aspx.cs" Inherits="penyewaan_ruangan.Admin.bayar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="row">
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		
			<div class="col-lg-12">
				<h2 class="page-header"><center>Halaman Catatan pembayaran</center></h2>
                 <marquee>Sistem Informasi Pengelolaan penyewaan ruangan</marquee>
			</div>

             <div class="col-lg-12">
                <div class="form-group">
                <label for="exampleInputPassword1">No Bayar</label>
                    <asp:TextBox ID="no" runat="server" class="form-control" placeholder="No Bayar" Width="300px" ></asp:TextBox>
                </div>
                <div class="form-group">
                 <label for="exampleInputPassword1">No Sewa</label>
                 <asp:TextBox ID="nosewa" runat="server" class="form-control"  placeholder="No Sewa"  Width="150px" ></asp:TextBox>
                 <br /> <asp:Button ID="cari" runat="server" Text="Cari" class="btn btn-info" 
                        Width="67px" onclick="cari_Click"/>
                </div>

                <div class="form-group">
                <label for="exampleInputPassword1">tanggal</label>
                    <asp:TextBox ID="tanggal" runat="server" class="form-control" TextMode="Date" Width="300px" ></asp:TextBox>
                </div>
                <div class="form-group">
                <label for="exampleInputPassword1">Jumlah Bayar</label>
                    <asp:TextBox ID="jumlah"  runat="server" class="form-control" placeholder="Jumlah Bayar" Width="300px" ></asp:TextBox>
                </div>
                 <asp:Label ID="noruangan" runat="server" Text="Label"></asp:Label>
                <asp:Button ID="btnsimpan" runat="server" Text="Bayar" class="btn btn-success" 
                     onclick="btnsimpan_Click" Width="95px"/>
                 <asp:Label ID="info" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="col-lg-12">
            <div id="print-area">
                <h3 class="page-header"><center>Bukti Pembayaran</center></h3>
                <asp:ListView ID="buktipembayaran" runat="server">
            <LayoutTemplate>
            <div class="table-responsive">
                <table id="artikel" class="table table-striped">
                    <thead>
                    <th class="info">No Bayar</th>
                    <th class="info">No Penyewaan</th>
                     <th class="info">Nama Customer</th>
                    <th class="info">Tanggal Bayar</th>
                    <th class="info">Jumlah Bayar</th>
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </div>
                </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    
                    <td><%# Eval("no_bayar")%></td>
                    <td><%# Eval("no_sewa")%></td>
                     <td><%# Eval("nama_customer")%></td>
                    <td><%# Eval("tgl_bayar")%></td>
                    <td><%# Eval("jml_bayar")%></td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
            </div>
            </div>
            <button type="button" onclick="printDiv('print-area')" class="btn btn-primary pull-right"><i class="fa fa-print"></i> Cetak
          </button>
		</div><!--/.row-->
</div>
</asp:Content>
