﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace penyewaan_ruangan.Admin
{
    public partial class Customer : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksi"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["username"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            tampilcustomer();
        }

        protected void btnsimpan_Click(object sender, EventArgs e)
        {
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"INSERT INTO [customer]
                               ([username]
                               ,[password]
                               ,[nama_customer]
                               ,[alamat]
                               ,[no_telpon]
                               ,[no_ktp])VALUES('"+username.Text+"','"+password.Text+"','"+nama.Text+"','"+alamat.Text+"','"+telpon.Text+"','"+ktp.Text+"')";
            cmd.ExecuteNonQuery();
            username.Text = "";
            password.Text = "";
            nama.Text = "";
            alamat.Text = "";
            telpon.Text = "";
            ktp.Text = "";
            conn.Close();
            tampilcustomer();
        }

        public void tampilcustomer()
        {
            string sql = @"SELECT [id_customer]
                              ,[username]
                              ,[password]
                              ,[nama_customer]
                              ,[alamat]
                              ,[no_telpon]
                              ,[no_ktp]
                          FROM [customer]";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            listCustomer.DataSource = dt;
            listCustomer.DataBind();
            cmd.Dispose();
            conn.Close();
        }

        protected void Inkpilih_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["id_customer"] = btn.CommandArgument;
            conn.Open();
            string sql = @"SELECT * FROM [customer] where [id_customer]='" + Session["id_customer"] + "'";
            cmd = new SqlCommand(sql, conn);
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    username.Text = dr[1].ToString();
                    password.Text = dr[2].ToString();
                    nama.Text = dr[3].ToString();
                    alamat.Text = dr[4].ToString();
                    telpon.Text = dr[5].ToString();
                    ktp.Text = dr[5].ToString();
                }
            }
        }

        protected void btnubah_Click(object sender, EventArgs e)
        {
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"UPDATE [customer] SET [username] = '"+ username.Text +"' ,[password] = '"+ password.Text+"' ,[nama_customer] = '"+nama.Text+"',[alamat] = '"+alamat.Text+"',[no_telpon] = '"+telpon.Text+"',[no_ktp] = '"+ktp.Text+"' WHERE [id_customer]='" + Session["id_customer"] + "'";
            cmd.ExecuteNonQuery();
            username.Text = "";
            password.Text = "";
            nama.Text = "";
            alamat.Text = "";
            telpon.Text = "";
            ktp.Text = "";
            conn.Close();
            tampilcustomer();
        }

        protected void btnhapus_Click(object sender, EventArgs e)
        {
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"DELETE FROM [customer] WHERE [id_customer]='" + Session["id_customer"] + "'";
            cmd.ExecuteNonQuery();
            username.Text = "";
            password.Text = "";
            nama.Text = "";
            alamat.Text = "";
            telpon.Text = "";
            ktp.Text = "";
            conn.Close();
            tampilcustomer();
        }
    }
}