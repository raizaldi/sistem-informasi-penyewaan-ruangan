﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace penyewaan_ruangan.Admin
{
    public partial class Ruangfasilitas : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksi"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["username"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            tampilruang();
        }


        public void tampilruang()
        {
            string sql = @"SELECT [no_ruangan]
                      ,[nama_ruangan]
                      ,[harga_ruangan]
                      ,[foto]
                  FROM [ruangdanfasilitas]";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            ListFasilitas.DataSource = dt;
            ListFasilitas.DataBind();
            cmd.Dispose();
            conn.Close();
        }

        protected void Inkpilih_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["no_ruangan"] = btn.CommandArgument;
            conn.Open();
            string sql = @"SELECT * FROM [ruangdanfasilitas] where [no_ruangan]='" + Session["no_ruangan"] + "'";
            cmd = new SqlCommand(sql, conn);
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    no.Text = dr[0].ToString();
                    nama.Text = dr[1].ToString();
                    harga.Text = dr[2].ToString();
                }
            }
        }

        public bool CekTipeFile(string fileName)
        {
            string ekstensi = Path.GetExtension(fileName).ToLower();
            switch (ekstensi)
            {
                case ".gif":
                case ".jpg":
                case ".jpeg":
                case ".png":
                    return true;
                default:
                    return false;
            }
        }

        protected void btnsimpan_Click(object sender, EventArgs e)
        {
            if (uploadgambar.HasFile)
            {
                if (CekTipeFile(uploadgambar.FileName))
                {
                    string strUpload = Path.Combine("~/images/", uploadgambar.FileName);
                    strUpload = MapPath(strUpload);
                    uploadgambar.SaveAs(strUpload);
                }
            }

            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"INSERT INTO [ruangdanfasilitas]
                               ([no_ruangan]
                               ,[nama_ruangan]
                               ,[harga_ruangan]
                               ,[foto])
                         VALUES
                               ('"+no.Text+"','"+nama.Text+"','"+harga.Text+"','"+uploadgambar.FileName+"')";
            cmd.ExecuteNonQuery();
            no.Text = "";
            nama.Text = "";
            harga.Text = "";
            conn.Close();
            tampilruang();
        }

        protected void btnubah_Click(object sender, EventArgs e)
        {
            if (uploadgambar.HasFile)
            {
                if (CekTipeFile(uploadgambar.FileName))
                {
                    string strUpload = Path.Combine("~/images/", uploadgambar.FileName);
                    strUpload = MapPath(strUpload);
                    uploadgambar.SaveAs(strUpload);
                }
            }

            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"UPDATE [ruangdanfasilitas] SET [nama_ruangan] = '" + nama.Text + "' ,[harga_ruangan] = '" + harga.Text + "' ,[foto] = '" + uploadgambar.FileName + "' WHERE [no_ruangan] = '" + Session["no_ruangan"] + "'";
            cmd.ExecuteNonQuery();
            no.Text = "";
            nama.Text = "";
            harga.Text = "";
            conn.Close();
            tampilruang();
        }

        protected void btnhapus_Click(object sender, EventArgs e)
        {
            if (uploadgambar.HasFile)
            {
                if (CekTipeFile(uploadgambar.FileName))
                {
                    string strUpload = Path.Combine("~/images/", uploadgambar.FileName);
                    strUpload = MapPath(strUpload);
                    uploadgambar.SaveAs(strUpload);
                }
            }

            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"DELETE FROM [ruangdanfasilitas] WHERE [no_ruangan] = '" + Session["no_ruangan"] + "'";
            cmd.ExecuteNonQuery();
            no.Text = "";
            nama.Text = "";
            harga.Text = "";
            conn.Close();
            tampilruang();
        }
    }
}