﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Login.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="penyewaan_ruangan.Admin.Login1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Halaman Login</div>
				<div class="panel-body">
					<form role="form">
						<fieldset>
							<div class="form-group">
                                <asp:TextBox ID="username" class="form-control" runat="server" autofocus="" placeholder="Username"></asp:TextBox>
							</div>
							<div class="form-group">
								<asp:TextBox ID="password" class="form-control" runat="server" placeholder="Password"></asp:TextBox>
							</div>
                            <asp:Button ID="Button1" runat="server" Text="Login" class="btn btn-primary" 
                                onclick="Button1_Click"/>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	

</asp:Content>
