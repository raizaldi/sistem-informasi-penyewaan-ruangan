﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Laporan.aspx.cs" Inherits="penyewaan_ruangan.Admin.Laporan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="row">
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
<div id="print-area">		
			<div class="col-lg-12">
				<h2 class="page-header"><center>Laporan Pembayaran Penyewaan</center></h2>
			</div>

         <div class="col-lg-12">
            <asp:ListView ID="ListJadwal" runat="server">
            <LayoutTemplate>
                <table class="table table-bordered">
                    <thead>
                    <th class="info"><center>No Bayar</center></th>
                    <th class="info"><center>No Sewa</center></th>
                    <th class="info"><center>Tanggal Bayar</center></th>
                    <th class="info"><center>Jumlah bayar</center></th>
                   
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td align="center"><%# Eval("no_bayar")%></td>
                    <td align="center"><%# Eval("no_penyewaan")%></td>
                    <td align="center"><%# Eval("tgl_bayar")%></td>
                    <td align="center"><%# Eval("jml_bayar")%></td>
                    
                </tr>
                
               
            </ItemTemplate>
        </asp:ListView>

            </div>


            <div class="col-lg-12">
            <asp:ListView ID="listTotal" runat="server">
            <LayoutTemplate>
                <table class="table table-striped">
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><center><b>Total</b></center></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>Rp.<%# Eval("total")%></b></td>
                </tr>
               
            </ItemTemplate>
        </asp:ListView>

            </div>
       
</div>
<div class="col-lg-12">
 <button type="button" onclick="printDiv('print-area')" class="btn btn-success pull-right"><i class="fa fa-print"></i> Print atau cetak pdf
          </button>
</div>
</div><!--/.row-->

</div>


</asp:Content>

