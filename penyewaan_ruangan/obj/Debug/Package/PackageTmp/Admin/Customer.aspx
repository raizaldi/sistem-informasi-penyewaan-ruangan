﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="penyewaan_ruangan.Admin.Customer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="row">
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		
			<div class="col-lg-12">
				<h2 class="page-header"><center>Data Master Customer</center></h2>
                <marquee>Sistem Informasi Pengelolaan penyewaan ruangan</marquee>
			</div>
            <div class="col-lg-4">
                <div class="form-group">
                    <asp:TextBox ID="nama" runat="server" class="form-control" placeholder="Nama Lengkap" Width="300px" autofocus></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="alamat" TextMode="MultiLine" runat="server" class="form-control" placeholder="Alamat" Width="300px" ></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="ktp" runat="server" class="form-control" placeholder="No Ktp" Width="300px" ></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="telpon" runat="server" class="form-control" placeholder="No Telpon" Width="300px" ></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="username" runat="server" class="form-control" placeholder="Username" Width="300px" ></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="password" runat="server" class="form-control" placeholder="password" Width="300px" ></asp:TextBox>
                </div>
                <asp:Button ID="btnsimpan" runat="server" Text="Simpan" class="btn btn-success" 
                    onclick="btnsimpan_Click" />
                &nbsp;
               <asp:Button ID="btnubah" runat="server" Text="Ubah" class="btn btn-warning" 
                    onclick="btnubah_Click" Width="84px" />
                 &nbsp;
               <asp:Button ID="btnhapus" runat="server" Text="Hapus" class="btn btn-danger" 
                    onclick="btnhapus_Click" />
            </div>
            <div class="col-lg-8">
         <asp:ListView ID="listCustomer" runat="server">
            <LayoutTemplate>
            <div class="table-responsive">
                <table id="artikel" class="table table-striped">
                    <thead>
                    
                    <th class="info">Nama</th>
                    <th class="info">Alamat</th>
                    <th class="info">No Ktp</th>
                    <th class="info">No Telpon</th>
                    <th class="info">username</th>
                    <th class="info">password</th>
                    <th class="info">ACTION</th>
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </div>
                </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    
                    <td><%# Eval("nama_customer")%></td>
                    <td><%# Eval("alamat")%></td>
                    <td><%# Eval("no_ktp")%></td>
                       <td><%# Eval("no_telpon")%></td>
                    <td><%# Eval("username")%></td>
                    <td><%# Eval("password")%></td>
                    <td>
                        <asp:LinkButton class="btn btn-info" ID="Inkselect" runat="server" CommandArgument='<%# Eval("id_customer")%>' OnClick="Inkpilih_Click" >Pilih</asp:LinkButton>
                                                          
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
            </div>
		</div><!--/.row-->
</div>
</asp:Content>
