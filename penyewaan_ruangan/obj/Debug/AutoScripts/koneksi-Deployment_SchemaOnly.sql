SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ruangdanfasilitas](
	[no_ruangan] [varchar](5) COLLATE Latin1_General_CI_AS NOT NULL,
	[nama_ruangan] [varchar](30) COLLATE Latin1_General_CI_AS NULL,
	[harga_ruangan] [decimal](9, 0) NULL,
	[foto] [varchar](50) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK__ruangdan__69B1199E0425A276] PRIMARY KEY CLUSTERED 
(
	[no_ruangan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[penyewaan](
	[no_penyewaan] [int] IDENTITY(1,1) NOT NULL,
	[no_ruangan] [varchar](5) COLLATE Latin1_General_CI_AS NULL,
	[tgl_pemesanan] [date] NULL,
	[id_customer] [int] NULL,
	[total_bayar] [decimal](9, 0) NULL,
	[keterangan] [varchar](50) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK__penyewaa__D73745AE07F6335A] PRIMARY KEY CLUSTERED 
(
	[no_penyewaan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[jadwal](
	[no_jadwal] [int] IDENTITY(1,1) NOT NULL,
	[no_ruangan] [varchar](5) COLLATE Latin1_General_CI_AS NULL,
	[no_bayar] [int] NULL,
	[keterangan] [varchar](50) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK__jadwal__8BB645A40F975522] PRIMARY KEY CLUSTERED 
(
	[no_jadwal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customer](
	[id_customer] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) COLLATE Latin1_General_CI_AS NULL,
	[password] [varchar](100) COLLATE Latin1_General_CI_AS NULL,
	[nama_customer] [varchar](50) COLLATE Latin1_General_CI_AS NULL,
	[alamat] [text] COLLATE Latin1_General_CI_AS NULL,
	[no_telpon] [varchar](13) COLLATE Latin1_General_CI_AS NULL,
	[no_ktp] [varchar](20) COLLATE Latin1_General_CI_AS NULL,
PRIMARY KEY CLUSTERED 
(
	[id_customer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bayar](
	[no_bayar] [int] NOT NULL,
	[no_penyewaan] [int] NULL,
	[tgl_bayar] [date] NULL,
	[jml_bayar] [decimal](9, 0) NULL,
 CONSTRAINT [PK__bayar__CFC7C0880BC6C43E] PRIMARY KEY CLUSTERED 
(
	[no_bayar] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

GO
