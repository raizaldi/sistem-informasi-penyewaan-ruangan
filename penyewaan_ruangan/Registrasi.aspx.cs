﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;


namespace penyewaan_ruangan
{
    public partial class Registrasi : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksi"].ToString();
            conn = new SqlConnection(sqlconn);
            if (!IsPostBack) {
                info.Visible = false;
            }
        }

        protected void btnDaftar_Click(object sender, EventArgs e)
        {
            try {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [customer]
                                   ([username]
                                   ,[password]
                                   ,[nama_customer]
                                   ,[alamat]
                                   ,[no_telpon]
                                   ,[no_ktp])
                             VALUES
                                   ('"+username.Text+"','"+password.Text+"','"+nama.Text+"','"+alamat.Text+"','"+telpon.Text+"','"+ktp.Text+"')";
                cmd.ExecuteNonQuery();
                username.Text = "";
                password.Text = "";
                nama.Text = "";
                alamat.Text = "";
                telpon.Text = "";
                ktp.Text = "";
                info.Visible = true;
                info.Text = "Registrasi Berhasil";
                conn.Close();
            
            }
            catch (Exception) { }
        }
    }
}