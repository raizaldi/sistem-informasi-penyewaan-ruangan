﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace CRM_AQUA
{
    public partial class Default : System.Web.UI.Page
    {

        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksi"].ToString();
            conn = new SqlConnection(sqlconn);
            tampilan();
        }


        public void tampilan() {
            string sql = @"SELECT [no_ruangan],[nama_ruangan],[harga_ruangan],[foto] FROM [ruangdanfasilitas]";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            listfasilitas.DataSource = dt;
            listfasilitas.DataBind();
            cmd.Dispose();
            conn.Close();
        }
    }
}