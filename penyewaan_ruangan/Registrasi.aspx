﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registrasi.aspx.cs" Inherits="penyewaan_ruangan.Registrasi" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <div class="form-group">
    <label for="exampleInputEmail1">Nama</label>
      <asp:TextBox ID="nama" runat="server" class="form-control" placeholder="Nama Customer"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Alamat</label>
      <asp:TextBox ID="alamat" TextMode="MultiLine" runat="server" class="form-control" placeholder="Alamat Customer"></asp:TextBox>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">No Telpon</label>
      <asp:TextBox ID="telpon" runat="server" class="form-control" placeholder="No Telpon"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">No Ktp</label>
      <asp:TextBox ID="ktp" runat="server" class="form-control" placeholder="No Ktp"></asp:TextBox>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Username</label>
      <asp:TextBox ID="username" runat="server" class="form-control" placeholder="Username"></asp:TextBox>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Password</label>
      <asp:TextBox ID="password" TextMode="Password" runat="server" class="form-control" placeholder="Password"></asp:TextBox>
  </div>
    <asp:Button ID="btnDaftar" runat="server" Text="Daftar" class="btn btn-default" 
        onclick="btnDaftar_Click" />
    
    <asp:Label ID="info" runat="server" Text="Label"></asp:Label>
   

</asp:Content>
