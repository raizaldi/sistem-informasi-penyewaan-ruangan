﻿<%@ Page Title="" Language="C#" MasterPageFile="~/users/user.Master" AutoEventWireup="true" CodeBehind="Penyewaan.aspx.cs" Inherits="penyewaan_ruangan.users.Penyewaan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <div class="form-group">
    <label for="exampleInputEmail1">No Ruangan</label>
      <asp:TextBox ID="no" runat="server" class="form-control" placeholder="No Ruangan"  Width="400px" ToolTip="Masukan Kode Ruangan yang tersedia di menu Ruang dan Fasilitas"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Tanggal Pemesanan</label>
      <asp:TextBox ID="tgl" TextMode="Date" runat="server" class="form-control"  Width="400px" ToolTip="Atur tanggal untuk penyewaan ruangan"></asp:TextBox>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Id Customer</label>
      <asp:TextBox ID="id" runat="server" class="form-control" placeholder="Id Customer"  Width="400px" disabled></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Fasilitas</label>
      <asp:CheckBoxList ID="listFasilitas" runat="server">
      <asp:ListItem Value="Band">Band</asp:ListItem>
      <asp:ListItem Value="Sound System">Sound System</asp:ListItem>
      <asp:ListItem Value="Standing Mix">Standing Mix</asp:ListItem>
      <asp:ListItem Value="Projector">Projector</asp:ListItem>
      <asp:ListItem Value="Air Conditioner">Air Conditioner</asp:ListItem>
      </asp:CheckBoxList>

      <br />
      <asp:Button ID="cariharga" runat="server" Text="Lihat Harga"
            class="btn btn-info" onclick="cariharga_Click" />
  </div>
   
    <div class="form-group">
    <label for="exampleInputEmail1">Fasilitas Dipilih</label>
      <asp:TextBox ID="fasilitas" runat="server" class="form-control" placeholder="nama Fasilitas"  Width="400px" ToolTip="Nama Fasilitas dan harga baru terlihat setelah memilih fasilitas dan klik lihat harga" ></asp:TextBox>
      
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Fasilitas yang harus dibayar</label>
      <asp:TextBox ID="hargafasilitas" runat="server" class="form-control" placeholder="Harga Bayar fasilitas"  Width="400px" ></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Total Bayar</label>
      <asp:TextBox ID="total" runat="server" class="form-control" placeholder="Total Bayar" Width="400px" ></asp:TextBox>
  </div>
    <asp:Button ID="Button1" runat="server" Text="Simpan" class="btn btn-default" onclick="Button1_Click" />
    <asp:Label ID="info" runat="server" Text="Label"></asp:Label>
</asp:Content>
