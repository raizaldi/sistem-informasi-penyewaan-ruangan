﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;


namespace penyewaan_ruangan.users
{
    public partial class Penyewaan : System.Web.UI.Page
    {

        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn, hasil, tampil, kode, fas, hrg;
        int hitung, jum;

        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksi"].ToString();
            conn = new SqlConnection(sqlconn);
            tampilHarga();
            if (!IsPostBack)
            {
                info.Visible = false;
            }

            

            if (Session["username"] == null)
            {
                Response.Redirect("../Default.aspx");
            }
            id.Text = Session["id_customer"].ToString();

        }


        public void tampilHarga()
        {
            try
            {
                conn.Open();
                string sql = @"SELECT [no_ruangan],[nama_ruangan],[harga_ruangan] FROM [ruangdanfasilitas] where [no_ruangan] = '" + no.Text + "'";
                cmd = new SqlCommand(sql, conn);
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        tampil = dr["harga_ruangan"].ToString();

                    }
                }
                conn.Close();

            }
            catch (Exception ex)
            {
                no.Text = ex.Message;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (no.Text != "" || tgl.Text != "" || id.Text != "" || fasilitas.Text != "")
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = @"INSERT INTO [penyewaan]
                                       ([no_ruangan]
                                       ,[tgl_pemesanan]
                                       ,[id_customer]
                                       ,[total_bayar]
                                       ,[keterangan])
                                 VALUES
                                       ('" + no.Text + "','" + tgl.Text + "','" + Session["id_customer"].ToString() + "','" + total.Text + "','" + fasilitas.Text + "')";
                    cmd.ExecuteNonQuery();
                    no.Text = "";
                    tgl.Text = "";
                    id.Text = "";
                    total.Text = "";
                    fasilitas.Text = "";
                    hargafasilitas.Text = "";
                    info.Visible = true;
                    info.Text = "Data berhasil tersimpan";
                    conn.Close();
                }
                {
                    info.Visible = true;
                    info.Text = "Data berhasil tersimpan dan silahkan tunggu untuk dihubungi";
                }
            }
            catch (Exception ex)
            {
                no.Text = ex.Message;
            }

        }

        public void cek()
        {
            foreach (ListItem item in listFasilitas.Items)
            {
                if (item.Selected)
                {
                    hasil += item.Text + ",";
                    fasilitas.Text = hasil;
                }
            }
        }

        protected void cariharga_Click(object sender, EventArgs e)
        {
            tampilHarga();
            cek();

            if (fasilitas.Text == "Band,") //1
            {
                hitung = 2000000;
            }
            else if (fasilitas.Text == "Sound System,") //2
            {
                hitung = 700000;
            }
            else if (fasilitas.Text == "Standing Mix,") //3
            {
                hitung = 80000;
            }
            else if (fasilitas.Text == "Projector,") //4
            {
                hitung = 200000;
            }
            else if (fasilitas.Text == "Air Conditioner,") //5
            {
                hitung = 1000000;
            }
            else if (fasilitas.Text == "Band,Sound System,") //1,2
            {
                hitung = 2700000;
            }
            else if (fasilitas.Text == "Band,Sound System,Standing Mix,") //1,2,3
            {
                hitung = 2780000;
            }
            else if (fasilitas.Text == "Band,Sound System,Standing Mix,Projector,") //1,2,3,4
            {
                hitung = 2980000;
            }
            else if (fasilitas.Text == "Band,Sound System,Standing Mix,Projector,Air Conditioner,") //1,2,3,4,5
            {
                hitung = 3980000;
            }
            else if (fasilitas.Text == "Sound System,Standing Mix,") //2,3
            {
                hitung = 780000;
            }
            else if (fasilitas.Text == "Band,Standing Mix,") //2,3,4
            {
                hitung = 980000;
            }
            else if (fasilitas.Text == "Sound System,Standing Mix,Projector,Air Conditioner,") //2,3,4,5
            {
                hitung = 1980000;
            }
            else if (fasilitas.Text == "Band,Air Conditioner,") //3,4
            {
                hitung = 3000000;
            }
            hargafasilitas.Text = hitung.ToString();
            if (tampil == null)
            {
                no.Text = "Nomer Ruangan harus diisi";
            }
            else
            {
                int a = int.Parse(tampil);
                int b = int.Parse(hargafasilitas.Text);
                jum = a + b;
                total.Text = jum.ToString();
            }
        }

    }
}