﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace penyewaan_ruangan.users
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             if (Session["nama_customer"] == null)
            {
                Response.Redirect("../Default.aspx");
            }
            Session.Clear();
            Response.Redirect("../Default.aspx");
        }
        }
    }
