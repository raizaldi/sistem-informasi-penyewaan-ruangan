create database penyewaan_ruangan
go
USE [penyewaan_ruangan]
GO
/****** Object:  Table [dbo].[ruangdanfasilitas]    Script Date: 06/29/2016 00:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ruangdanfasilitas](
	[no_ruangan] [varchar](5) NOT NULL,
	[nama_ruangan] [varchar](30) NULL,
	[harga_ruangan] [decimal](9, 0) NULL,
	[foto] [varchar](50) NULL,
 CONSTRAINT [PK__ruangdan__69B1199E0425A276] PRIMARY KEY CLUSTERED 
(
	[no_ruangan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[penyewaan]    Script Date: 06/29/2016 00:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[penyewaan](
	[no_penyewaan] [int] IDENTITY(1,1) NOT NULL,
	[no_ruangan] [varchar](5) NULL,
	[tgl_pemesanan] [date] NULL,
	[id_customer] [int] NULL,
	[total_bayar] [decimal](9, 0) NULL,
	[keterangan] [varchar](50) NULL,
 CONSTRAINT [PK__penyewaa__D73745AE07F6335A] PRIMARY KEY CLUSTERED 
(
	[no_penyewaan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[jadwal]    Script Date: 06/29/2016 00:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[jadwal](
	[no_jadwal] [int] IDENTITY(1,1) NOT NULL,
	[no_ruangan] [varchar](5) NULL,
	[no_bayar] [int] NULL,
	[keterangan] [varchar](50) NULL,
 CONSTRAINT [PK__jadwal__8BB645A40F975522] PRIMARY KEY CLUSTERED 
(
	[no_jadwal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[customer]    Script Date: 06/29/2016 00:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customer](
	[id_customer] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](100) NULL,
	[nama_customer] [varchar](50) NULL,
	[alamat] [text] NULL,
	[no_telpon] [varchar](13) NULL,
	[no_ktp] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_customer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[bayar]    Script Date: 06/29/2016 00:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bayar](
	[no_bayar] [int] NOT NULL,
	[no_penyewaan] [int] NULL,
	[tgl_bayar] [date] NULL,
	[jml_bayar] [decimal](9, 0) NULL,
 CONSTRAINT [PK__bayar__CFC7C0880BC6C43E] PRIMARY KEY CLUSTERED 
(
	[no_bayar] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
